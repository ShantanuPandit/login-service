package org.bookdumps.poc.loginservice.controller;

import java.util.List;

import org.bookdumps.poc.loginservice.bean.User;
import org.bookdumps.poc.loginservice.bean.Users;
import org.bookdumps.poc.loginservice.service.LoginServiceInterface;
import org.bookdumps.poc.loginservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private LoginServiceInterface service;
	
	@Value("${APPLICATION.NAME:LOCAL}")
	private String configProp;
	
	@GetMapping("/users")
	public Users getUsers(){
		Users users = new Users();
		users.setEnv(configProp);
		users.setUsers(this.userService.getAllUsers());
		return users;
	}
	
	@GetMapping("/login/{userName}/{pwd}")
	public User getLogin(@PathVariable String userName,@PathVariable String pwd){
		User user = new User();
		if(this.service.validateLogin(userName,pwd)) {
			user = this.userService.getValidUser(userName);
		}
		return user;
	}
}