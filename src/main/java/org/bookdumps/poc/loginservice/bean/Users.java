package org.bookdumps.poc.loginservice.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Users {

	@JsonIgnore
	private String env;

	private List<User> users;

	@JsonProperty("Env =>")
	public String getConfigEnv() {
		return this.env;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

}