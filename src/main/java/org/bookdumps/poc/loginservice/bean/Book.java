package org.bookdumps.poc.loginservice.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Book {


	@Id
	@GeneratedValue
	@JsonIgnore
	private Integer bookId;
	private String bookName;
	private String author;
	private Integer rating;
	private Integer price;

	public Book(Integer bookId, String bookName, String author, Integer rating, Integer price) {
		super();
		this.bookId = bookId;
		this.bookName = bookName;
		this.author = author;
		this.rating = rating;
		this.price = price;
	}

	public Book() {
		super();
	}
	
	public String getCustomMsg() {
		return "Good Book, Must read ==>"+this.bookName;
	}

	public Integer getBookId() {
		return bookId;
	}

	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	@JsonProperty("Price($USD)")
	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

}
