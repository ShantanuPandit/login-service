package org.bookdumps.poc.loginservice.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Login {
	
	@Id
	private String userName;	
	private String password;
	private Integer userId;
	
	public Login() {
		super();
	}
	public Login(String userName, String password, Integer userId) {
		super();
		this.userName = userName;
		this.password = password;
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
}
