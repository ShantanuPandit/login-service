package org.bookdumps.poc.loginservice.bean;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {

	@Id
	@GeneratedValue
	private Integer userId;
	private String userName;
	private String fullName;
	@JsonIgnore
	private String password;
	@JsonIgnore
	@Column(name = "book_ids")
	private String bookIds;
	@Transient
	private List<Book> userBooks;

	public User() {
		super();
	}

	public User(Integer userId, String userName, String fullName, String password, String bookIds,
			List<Book> userBooks) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.fullName = fullName;
		this.password = password;
		this.bookIds = bookIds;
		this.userBooks = userBooks;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Book> getUserBooks() {
		return userBooks;
	}

	public void setUserBooks(List<Book> userBooks) {
		this.userBooks = userBooks;
	}

	public String getBookIds() {
		return bookIds;
	}

	public void setBookIds(String bookIds) {
		this.bookIds = bookIds;
	}

}
