package org.bookdumps.poc.loginservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.bookdumps.poc.loginservice.bean.User;
import org.bookdumps.poc.loginservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BookServiceProxy bookServiceProxy;
	
	public List<User> getAllUsers() {
		List<User> users= new ArrayList<User>();
		users = this.userRepository.findAll();
		users.forEach((User u)->u.setUserBooks(bookServiceProxy.getBooksForUser(u.getBookIds())));
			return users;
	}
	
	public User getValidUser(String userName) {
		User validUser= new User();
		Optional<User> optionalUser = this.userRepository.findByuserName(userName);
		if(optionalUser.isPresent()) {
			validUser = optionalUser.get();
			validUser.setUserBooks(this.bookServiceProxy.getBooksForUser(optionalUser.get().getBookIds()));
		}
			return validUser;
	}

}
