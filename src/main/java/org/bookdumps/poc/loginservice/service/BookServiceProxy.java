package org.bookdumps.poc.loginservice.service;

import java.util.List;

import org.bookdumps.poc.loginservice.bean.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "book-service")
public interface BookServiceProxy {

	@GetMapping("/books/{bookIdList}")
	List<Book> getBooksForUser(@PathVariable String bookIdList);
}