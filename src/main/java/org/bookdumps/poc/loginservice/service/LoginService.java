package org.bookdumps.poc.loginservice.service;

import java.util.Optional;

import org.bookdumps.poc.loginservice.bean.Login;
import org.bookdumps.poc.loginservice.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Primary
public class LoginService implements LoginServiceInterface{

	@Autowired
	private LoginRepository loginRepository;

	public boolean validateLogin(String userName, String pwd) {
		boolean validUser = false;
		Optional<Login> optionalBean = this.loginRepository.findById(userName);
		if (optionalBean.isPresent()) {
			validUser= true;
		}
		return validUser;
	}
}