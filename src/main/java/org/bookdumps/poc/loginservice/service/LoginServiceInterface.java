package org.bookdumps.poc.loginservice.service;

public interface LoginServiceInterface {

	boolean validateLogin(String userName, String pwd);
}