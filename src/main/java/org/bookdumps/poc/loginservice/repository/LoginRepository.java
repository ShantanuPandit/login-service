package org.bookdumps.poc.loginservice.repository;

import org.bookdumps.poc.loginservice.bean.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends JpaRepository<Login, String> {

}
