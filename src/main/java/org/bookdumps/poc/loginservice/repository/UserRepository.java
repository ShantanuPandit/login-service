package org.bookdumps.poc.loginservice.repository;

import java.util.Optional;

import org.bookdumps.poc.loginservice.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	Optional<User> findByuserName(String userName);

}
