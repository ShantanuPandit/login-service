insert into user(user_id,user_name,full_name,password,book_ids) values(101, 'Shantanu','Shantanu Pandit','Shantanu101','1001_1002_1003_1004_1005_1005');
insert into user(user_id,user_name,full_name,password,book_ids) values(102, 'Abhinash','Abhinash Jha','Abhinash102','1001_1002_1003_1004_1010');
insert into user(user_id,user_name,full_name,password,book_ids) values(103, 'Anoop','Anoop Pradhan','Anoop103','1001_1002_1003_1009_1010');
insert into user(user_id,user_name,full_name,password,book_ids) values(104, 'Sudipto','Sudipto Sadhukhan','Sudipto104','1001_1002_1008_1009_1010');
insert into user(user_id,user_name,full_name,password,book_ids) values(105, 'Sadhu','Sadhu Mahapatra','Sadhu105','1001_1002_1007_1008_1009_1010');
insert into user(user_id,user_name,full_name,password,book_ids) values(106, 'Ashish','Ashish Nayak','Ashish106','1005_1006_1007_1008_1009_1010');
insert into user(user_id,user_name,full_name,password,book_ids) values(107, 'Sanjay','Sanjay Damodaran','Sanjay107','1001_1002_1003_1004_1010');
insert into user(user_id,user_name,full_name,password,book_ids) values(108, 'Lenin','Lenin Manivannan','Lenin108','1001_1002_1006_1008_1009_1010');



insert into login(user_name,password,user_id) values('Shantanu','Shantanu101',101);
insert into login(user_name,password,user_id) values('Abhinash','Abhinash102',102);
insert into login(user_name,password,user_id) values('Anoop','Anoop103',103);
insert into login(user_name,password,user_id) values('Sudipto','Sudipto104',104);
insert into login(user_name,password,user_id) values('Sadhu','Sadhu105',105);
insert into login(user_name,password,user_id) values('Ashish','Ashish106',106);
insert into login(user_name,password,user_id) values('Sanjay','Sanjay107',107);
insert into login(user_name,password,user_id) values('Lenin','Lenin108',108);